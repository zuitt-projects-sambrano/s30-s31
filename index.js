// Note: all packages/modules should be required at the top of the file to avoid tampering or errors

const express = require('express');

// Mongoose is a package that uses an ODM or object document mapper. It allows us to translate our JS objects into database documents for MongoDB. It allos connection and easier manipulation of our documents in MongoDB

const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes');

const userRoutes = require('./routes/userRoutes');

const port = 4000;

const app = express();

/*
    mongoose.connect is the method to connect your API to your mongoDB via the use of mongoose. It has 2 arguments:

	>> First, is the connection string to connect our api to our mongodb atlas. 

	>> Second, is an object used to add information between mongoose and mongodb.

	>> replace/change <password> in the connection string to your db password

	>> replace/change myFirstDatabase to task169

	MongoDB upon connection and creating our first documents will create the task169 database for us.

    Syntax:
        mongoose.connect("<connectionStringFromMongoDBAtlas", {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
*/

mongoose.connect("mongodb+srv://admin_sambrano:admin169@batch-169.gdgwg.mongodb.net/task169?retryWrites=true&w=majority", {

    useNewUrlParser: true,
    useUnifiedTopology: true
});

// Create notification if the connection to the db is successful or not

let db = mongoose.connection;

// We add this so that when db has a connection error, we will show the connection error both in the terminal and in the browser for our client
db.on('error', console.error.bind(console, 'Connection Error'));

// Once the connection is open and successful, we will output a message in the terminal
db.once('open', () => console.log('Connected to MongoDB'));

app.use(express.json());

// our server will use a middleware to group all task routes under /tasks
// All the endpoints in taskRoutes file will start with /tasks
app.use('/tasks', taskRoutes);

app.use('/users', userRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`))